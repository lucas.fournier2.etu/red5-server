# RAPPORT DES MODIFICATIONS

## Introduction

Après analyse, nous avons remarqué que le projet **Red5Server** mérite quelques optimisations. Dans ce rapport, nous allons lister quels types d'optimisation il est pertinent d'appliquer à ce programme.

Nous allons passer en revue les différents commits de ce dépôt dans l'ordre chronologique.

## Dead Code

### Suppression de la méthode `destroy()`

La classe `LoggerContextFilter` contenait une méthode `destroy()` vide. Elle aurait pu servir à implémenter une future fonctionnalité et déjà être utilisée ailleurs dans le code. 

Cependant, elle n'avait aucune référence et aucun implémentation. J'ai donc fais le choix de la supprimer étant donnée l'absence de commentaire indiquant son utilité présente ou future. Cela a permis à la classe de réduire sa taille, sa lisibilité, et d'améliorer le taux de deadCode.

### Suppresion de la méthode `pushMessage()`

La classe `InMemoryPullPullPipe` contenait également une méthode vide et qui n'était utilisée nulle part dans le reste du programme.

En revanche, cette fois, la méthode comprenait un commentaire explicite informant que l'action réalisée à la base par cette méthode doit être ignorée. J'ai donc choisis de supprimer définitivement cette méthode étant donnée son absence d'impact. De la même manière que pour la suppression précédente, cela a permis d'améliorer la lisibilité de la classe, réduire sa taille et baisser le taux de deadCode.

## Découpage de la méthode `sessionCreated()`

La classe `DebugProxyHandler` contenait une grande méthode multifonction qui pouvait largement être optimisée. Elle réalisait plusieurs tâche successivement et ne représentait pas un intérêt suffisant pour justifier sa taille.

J'ai donc décidé de la découper en plus petites méthodes. Cela permet de diviser et de hiérarchiser les responsabilités, de réduire les dépendances des implémentations ainsi que la charge de cette unique méthode. Pour finir, cela réduit le code smells, particulièrement important dans certaines classes du projet.

### Création de la méthode `isNotClient()`

Cette méthode comprenait une partie de test de la nullité d'un objet. J'ai décidé de mettre ce code dans une nouvelle méthode avec un nom très explicite, dans le but de faciliter la lecture. Cela rend la méthode `sessionCreated()` déjà un peu plus claire.

### Création de la méthode `isDebugEnable()`

La méthode sessionCreated possédait une partie trop importante dédiée à la vérification de l'activation du debug. Cette partie a été envoyée vers une nouvelle méthode au nom explicite. Cela a permis d'améliorer grandement la lecture de la méthode, et surtout de retirer beaucoup de lignes sans grand intérêt.

### Création de la méthode `headerConfig()`

Il y avait une grande partie dédiée à la configuration des différents éléments du header. On pouvait clairement distinguer une fonction à part entière, qui pouvait largement être donnée à une autre méthode. Cette nouvelle fonction a permis d'améliorer de Clean Code en divisant les responsabilité et surtout la taille de `sessionCreated()`.

## Découpage de la méthode `decodeCalls()`

Dans la classe `RemotingProtocolDecoder`, on peut trouver une méthode nommée `decodeCalls()`. Dans le même esprit qu'`headerConfig()`, elle rassemblait beaucoup trop de fonctionnalités et de responsabilité. Découper cette fonction en plusieurs méthode plus petites m'a semblé essentielle pour la lecture de cette imposante classe. De plus, il sera possible de diviser les responsabilités afin de laisser la possibilités d'appeler individuellement chaque méthode si besoin. Cela améliore grandement le Clean Code. 

### Création de la méthode `decode()`

Dans un premier temps, j'ai pu discerner une partie qui se détachait du reste de part sa fonction. La méthode `decodeCalls()` contenait un bloc entier qui se chargeait uniquement de "décoder" l'appel. La méthode `decode()` a donc été créée afin de séparer les différentes fonctionnalités de cette grande méthode. De part ce changement, la classe est devenue vraiment plus lisible et compréhensible, même sans commentaires.

### Création de la méthode `serviceConfig()`

Un problème est apparu à cause de la modification précédente. Effectivement, la méthode `decodeCalls()` était améliorée, mais les problèmes, bien que réduits, ont été transférés à la méthode `decode()`.

Dans le but d'améliorer encore davantage la lisibilité et la répartition des responsabilités et de la charge, la méthode `serviceConfig()` a été créée. Cette méthode permet d'exporter hors de `decode()` quelques lignes du code qui permet de configurer les variables de type service. Cette fonction s'est donc retrouvée séparée des autres, ce qui pourra permettre de n'invoquer qu'elle et d'éviter le code redondant lors d'un possible ajout de fonctionnalité. 

Cette nouvelle méthode a donc permis d'augmenter la robustesse de la classe en divisant les responsabilités, d'améliorer le Clean Code en séparant les fonctionnalités dans une classe dédiée, et enfin d'augmenter encore davantage la lisibilité.

### Création de la méthode `amf3Config()`

Dans le même esprit que `serviceConfig()`, j'ai créé une nouvelle méthode `amf3Config()` qui permet de séparer du reste la configuration des différents objets en AMF3. En effet malgré la séparation des fonctionnalités il reste encore certaines fonctionnalités qui se retrouvent trop dépendantes les unes des autres.

### Ajout d'attributs de classe et de Setter

A cause de la division d'une méthode très importante en terme de taille et de fonctionnalités, on peut observer très rapidement une multiplication rapide des paramètres des méthodes, parfois au delà de 3.

J'ai donc choisi de transformer certaines variables locales propres à la fonction en attributs de classe, dans le but que toutes les méthodes puissent y accéder et les modifier. Afin de compléter cette modification, j'ai eu besoin de créer un Setter pour une variable bien précise, dans l'objectif que sa valeur reste à jour.

## Création de la méthode `serviceInvokeIsNull()`

### Généralités de la classe

La classe `FlexMessagingService` contient une très grande méthode `handleRequest`. Cette méthode est remplie de diverses fonctionnalités, est très peu commentée et très peu lisible. De manière générale, la classe toute entière est écrite ainsi.

### Création de la méthode

Pour montrer les changements possibles tout en évitant la répétition de cette modification, j'ai créé la méthode `serviceInvokeIsNull()` qui illustre l'explication. 

Cette méthode teste simplement si l'objet serviceInvoke est égal à `null`. Si c'est le cas, elle affiche un message log et applique un return à la fonction de manière à l'interrompre. 

Cependant, il est possible d'optimiser ce système en dédiant une petite fonction qui affiche les messages log et retourne un boolean indiquant le résultat. C'est la nature de `serviceInvokeIsNull()`, qui illustre le besoin de la méthode géante. 

Ce genre de petites fonctions permet de simplifier de manière significative la lisibilité de la méthode, voire de toute la classe. Elle permet également d'éviter le code redondant, avec l'apparition à plusieurs reprise du même test sur différentes variables.

## Conclusion

Pour conclure, ces différents points permettent d'illustrer le type d'optimisations applicables au projet **Red5Server**. 

La suppression pure et simple des nombreux Dead Code rend le projet bien plus lisible. Cependant, cette pratique n'est pas à généraliser car il arrive que certaines méthode soient présente en vue d'un futur ajout de fonctionnalité. Il est donc essentiel de vérifier l'implémentation des méthodes et de se référer aux commentaires, les rares fois où ils sont présents.

Le découpage des méthodes géante en fonction de leur fonctionnalités est aussi une optimisation très importante qui pourrait participer à améliorer grandement la lisibilité, la compactibilité, et la robustesse du code. Cela signifie que le code devient de bien meilleure qualité, ce qui pourrait même améliorer les performance du projet au vu de la quantité importante de méthodes géantes.

Enfin, l'exportation des conditions vers des petites méthodes aux noms explicites seraient un plus. Elle participe à améliorer la lisibilité du code, et à pallier l'absence de commentaire, ce qui est un point faible conséquent de ce programme.
